"""
Gateway between mosaik and the MAS.

The class :class:`MosaikAPI` implements the `high-level mosaik API`.

The MosaikAPI also manages the root main_container for the MAS.  It starts
a :class:`MosaikAgent`.  The MosaikAgent serves as a gateway between the
FlexAgents and mosaik.

The FlexAgents do not run within the root main_container but in separate
container in sub processes.  These subprocesses are as well managed by the
MosaikAPI.

The entry point for the MAS is the function :func:`main()`.  It parses the
command line arguments and starts the :func:`run()` coroutine which runs until
mosaik sends a *stop* message to the MAS.

.. _mosaik API:
   https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html

"""

import asyncio
import logging

import mosaik_api

from mango.core.agent import Agent
from mango.core.container import Container
from mango.messages.message import Performatives
from mango.role.core import RoleAgent

from mango_library.coalition.core import CoalitionParticipantRole

from mosaik_cohda.agent_roles import FlexReceiverRole, FlexCohdaRole, \
    FlexTerminationRole, FlexNegotiationStarterRole
from mosaik_cohda.StartValues import StartValues

logger = logging.getLogger('mas.mosaik')


def main():
    """Run the multi-agent system."""
    logging.basicConfig(level=logging.INFO)
    return mosaik_api.start_simulation(MosaikAPI())


# The simulator meta data that we return in "init()":
META = {
    'api_version': '3.0',
    'type': 'time-based',
    'models': {
        'FlexAgent': {
            'public': True,
            'params': ['control_id', 'time_resolution'],
            'attrs': ['Flexibility', 'FlexSchedules', 'StartValues'],
        },
    },
}


class MosaikAPI(mosaik_api.Simulator):
    """
    Interface to mosaik.
    """

    def __init__(self):
        super().__init__(META)
        # We have a step size of 15 minutes specified in seconds:
        self.step_size = 60 * 15  # seconds
        self.host = 'localhost'
        self.port = 5678

        # This future will be triggered when mosaik calls "stop()":
        self.stopped = asyncio.Future()

        # Set by "run()":
        self.mosaik = None  # Proxy object for mosaik

        self.loop = None

        # Set in "init()"
        self.sid = None  # Mosaik simulator IDs

        # The following will be set in init():
        self.main_container = None  # Root agent main_container
        self.agent_container = None  # Container for agents
        self.mosaik_agent = None

        # Updated in "setup_done()"
        self.agents = {}  # eid : ((host,port), aid)

        # Set/updated in "setup_done()"
        self.uids = {}  # agent_id: unit_id

        logging.basicConfig(filename='mas.log', filemode='w',
                            level=logging.INFO)

    def init(self, sid, time_resolution=1., **sim_params):

        # we have to get the event loop in order to call async functions
        # from init, create and step
        self.loop = asyncio.get_event_loop()
        self.sid = sid
        self.loop.run_until_complete(self.create_container_and_main_agents(
            **sim_params))
        return META

    async def create_container_and_main_agents(self):
        """
        Creates the mosaik agent
        :param controller_config: Configuration for the controller coming
        from the init call of mosaik
        """

        # Root main_container for the MosaikAgent
        self.main_container = \
            await Container.factory(addr=(self.host, self.port))

        # Start the MosaikAgent in the main_container
        self.mosaik_agent = MosaikAgent(self.main_container)

        # Create container for the FlexAgents
        self.agent_container = \
            await Container.factory(addr=(self.host, self.port + 1))

    def create(self, num, model, **model_conf):
        """Create *num* instances of *model* and return a list of entity dicts
        to mosaik."""

        assert model in META['models']
        entities = []
        # Get the number of agents created so far and count from this number
        # when creating new entity IDs:
        n_agents = len(self.mosaik_agent.agents)
        for i in range(n_agents, n_agents + num):
            # Entity data
            eid = 'Agent_%s' % i
            entities.append({'eid': eid, 'type': model})

            initiator = False
            if i == model_conf['control_id']:
                initiator = True
            agent = self.loop.run_until_complete(self.create_flex_agent(
                self.agent_container, model_conf[
                    'time_resolution'], initiator =initiator))

            # Store details in agents dict
            self.mosaik_agent.agents[eid] = (self.agent_container.addr,
                                             agent.aid)

        return entities

    async def create_flex_agent(self, container: Container,
                                time_resolution, initiator: bool=False) \
            -> Agent:

        a = RoleAgent(container)
        a.add_role(CoalitionParticipantRole())
        a.add_role(FlexCohdaRole(time_resolution=time_resolution))
        a.add_role(FlexReceiverRole((self.main_container,
                                     self.mosaik_agent.aid)))
        a.add_role(FlexTerminationRole())

        if initiator:
            a.add_role(FlexNegotiationStarterRole())

        return a

    def setup_done(self):
        """Get the entities that our agents are connected to once the scenario
        setup is done."""
        full_ids = ['%s.%s' % (self.sid, eid)
                    for eid in self.mosaik_agent.agents.keys()]
        relations = yield self.mosaik.get_related_entities(full_ids)
        for full_aid, units in relations.items():
            uid, _ = units.popitem()
            # Create a mapping "agent ID -> unit ID"
            aid = full_aid.split('.')[-1]
            self.uids[aid] = uid

    def finalize(self):
        """

        :return:
        """
        self.loop.run_until_complete(
            self._shutdown(self.mosaik_agent,
                           self.main_container, self.agent_container))

    @staticmethod
    async def _shutdown(*args):
        futs = []
        for arg in args:
            futs.append(arg.shutdown())
        print('Going to shutdown agents and container... ', end='')
        await asyncio.gather(*futs)
        print('done.')

    def step(self, time, inputs, max_advance):
        """Send the flexibility of the controlled unites to our agents and
        get new schedules for these units from the agents.

        This method will run for at most "step_size" seconds, even if the
        agents need longer to do their calculations.  They will then continue
        to do stuff in the background, while this method returns and allows
        mosaik to continue the simulation.
        """

        # Prepare input data and forward it to the agents:
        flex_data = {}
        start_data = {}
        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                assert len(values) == 1  # b/c we're only connected to 1 unit
                _, value = values.popitem()
                if attr == 'StartValues':
                    participant_list = []
                    for participant in value.participants:
                        participant_eid = 'Agent_%s' % participant
                        participant_list.append(tuple((
                            self.mosaik_agent.agents[participant_eid])))
                        # needs to be casted to tuple to be useful later on

                    start_data[eid] = StartValues(
                        schedule=value.schedule,
                        participants=participant_list)

                elif attr == 'Flexibility':
                    flex_data[eid] = value
                else:
                    raise AttributeError('Attribute "%s" not available' % attr)

        # trigger the loop to enable agents to send / receive messages via
        # run_until_complete
        output_dict = self.loop.run_until_complete(self.mosaik_agent.step(
            input_flex=flex_data, input_start_values=start_data))

        # Make "set_data()" call back to mosaik to send the set-points:
        inputs = {aid: {self.uids[aid]: {'FlexSchedules': schedule}}
                  for aid, schedule in output_dict.items()}
        yield self.mosaik.set_data(inputs)

        return time + self.step_size

    def get_data(self, outputs):
        # we are going to send the data asynchronously via set_data,
        # hence we do not need to implement get_data()
        pass


class MosaikAgent(Agent):
    """This agent is a gateway between the mosaik API and the FlexAgents.

    It forwards the current state of the simulated devices to the agents,
    triggers the controller, waits for the controller
    to be done and then collects new set-points for the simulated devices from
    the agents.
    """

    def __init__(self, container: Container):
        super().__init__(container)
        # Dict mapping mosaik eid to address (agent_addr, agent_id) (set in
        # mosaik api)
        self.agents = {}

        # Done if all new schedules arrived
        self._schedules = {}

    async def step(self, input_flex, input_start_values):
        """
        This will be called from the mosaik api once per step.

        :param input_flex: the input dict with flexibility from mosaik
        :param input_start_values: the input dict with start values from
        mosaik
        :return: the output dict: {eid_1: schedule, eid_2: schedule}
        """

        # 1. reset
        self.reset()
        logging.info('Reset MAS')

        # 2. update flex for agents
        await self.update_agents(input_flex)
        logging.info('Sent new flex to agents')

        # 3. update start values for agents (automatically start)
        await self.update_agents(input_start_values)
        logging.info('Sent new schedule to agents')

        # 4. get schedules from agents and return it
        schedules = await self.get_schedules()
        logging.info('Got new schedules from MAS')
        return schedules

    def handle_msg(self, content, meta):
        """
        :param content: Content of the message
        :param meta: Meta information
        """

        if meta['message_type'] == 'solution':
            aid = meta['sender_id']
            for k, value in self.agents.items():
                if value[1] == aid:
                    self._schedules[k].set_result(content)

    async def update_agents(self, data):
        """Update the agents with new data from mosaik."""
        futs = []
        for mosaik_eid, input_data in data.items():
            futs.append(
                self.schedule_instant_task(
                    self._container.send_message(
                        receiver_addr=self.agents[mosaik_eid][0],
                        receiver_id=self.agents[mosaik_eid][1],
                        content=input_data,
                        acl_metadata={'performative': Performatives.inform,
                                      'conversation_id': mosaik_eid,
                                      'sender_id': self.aid,
                                      'message_type': type(
                                          input_data).__name__},
                        create_acl=True,
                    )
                )
            )

        await asyncio.gather(*futs)

    async def get_schedules(self):
        """Collect new FlexSchedules from the agents and return
        them to the mosaik API."""

        # wait for all schedules
        await asyncio.gather(*[fut for fut in self._schedules.values()])

        # return schedule
        return {key: value.result() for key, value in self._schedules.items()}

    def reset(self):
        """
        :return:
        """
        self._schedules = {aid: asyncio.Future() for aid in self.agents.keys()}


if __name__ == '__main__':
    main()
