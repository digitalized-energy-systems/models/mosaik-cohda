from typing import NamedTuple


class StartValues(NamedTuple):
    schedule: list[float]
    participants: list
