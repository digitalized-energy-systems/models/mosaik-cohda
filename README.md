# mosaik-cohda

This model introduces COHDA as a simulator for a mosaik simulation. COHDA is available as mango-implementation at https://gitlab.com/mango-agents/mango-library/-/tree/development 

## Description
The simulator gets two inputs:
* a flexibility for each agent ([des_flex](https://gitlab.com/digitalized-energy-systems/models/des_flex/-/blob/main/flex_class.py))
* start values for the negotiation ([StartValues](https://gitlab.com/zdin-zle/models/mosaik-cohda/-/blob/main/mosaik_cohda/StartValues.py)):
   * target_schedule (as list)
   * participant_list (as list of agent eid)

The simulator performs a COHDA simulation. Therefore, each agents generates schedules based on the flexibility using [flex_cohda](https://gitlab.com/digitalized-energy-systems/models/flex-cohda), which is based on [eo_cohda](https://gitlab.com/digitalized-energy-systems/models/eo-cohda). The schedules are regularly updated based on the current state of the COHDA negotiation.

At the end, the simulator returns schedules for each agent.

## Installation
1. Clone this repository with `git clone`
2. Update all submodules with `git submodule update --init --recursive`
3. We recommend the use of pipenv. Install all requirements with `pipenv install`

## Usage
The [generell test](https://gitlab.com/zdin-zle/models/mosaik-cohda/-/blob/main/tests/generell_test.py) shows a good use case.

## Authors and acknowledgment
The COHDA simulator for mosaik was developed by Stephan Ferenz[^1] and Rico Schrage[^1].

## License
The code is distributed under the MIT license.

_____

[^1]: [Department for Computer Science, Carl von Ossietzky University of Oldenburg](https://uol.de/des)
