"""
Implementation of the `mosaik API`_ for the Pickle sim.

.. _mosaik API:
   https://mosaik.readthedocs.org/en/latest/mosaik-api/high-level.html

"""

import mosaik_api
from mosaik_cohda.StartValues import StartValues


# The meta data that the "init()" call will return.
META = {
    'api_version': '3.0',
    'type': 'time-based',
    'models': {
        'DummySim': {
            'public': True,
            'params': [],
            'attrs': [
                'StartValues',  # output
            ],
        },
    },
}
STEP_SIZE = 15  # minutes


class DummySim(mosaik_api.Simulator):
    """This class implements the mosaik API."""
    def __init__(self):
        # We need to pass the META data to the parent class which will extend
        # it and store it as "self.meta":
        super().__init__(META)

        self.dummies = {}
        self.object: StartValues

    def init(self, sid, time_resolution=1., **sim_params):
        """
        Create dummy object
        """
        schedule = [0] * 96
        # for i in [20, 21, 40, 57, 86, 90]:
        #     schedule[i] = 5

        self.object = StartValues(schedule=schedule, participants=[0, 3, 4, 7,
                                                                   8])

        # Return our simulator's meta data to mosaik:
        return self.meta

    def create(self, num, model, **pickles_params):
        """Create *num* instances of *model*.



        """
        existing_instances = len(self.dummies)  # Number of
        # pickle-simulators so far
        entities = []  # This will hold the entity data for mosaik
        for idx in range(existing_instances, existing_instances + num):
            # The entity ID for mosaik:
            eid = 'dummy-%s' % idx
            self.dummies[eid] = idx

            # Add entity data for mosaik
            entities.append({'eid': eid, 'type': model})

        # Return the list of entities to mosaik:
        return entities

    def setup_done(self):
        """Called once the scenario creation is done and just before the
        simulation starts.

        """

    def step(self, time, inputs, max_advance):
        """Step the simulator ahead in time.  The current simulation time is
        *time*.

        *inputs* may contain *P_Max* values set by the MAS.  It is a dict
        like::


        """
        # No Computation, output stays the same for all steps

        # We want to do our next step in STEP_SIZE minutes:
        return time + (STEP_SIZE * 60)

    def get_data(self, outputs):
        """Return the data requested by mosaik.  *outputs* is a dict like::

            {
                'dummy-0': ['v', 'P'],
                ...
            }

        """
        data = {}
        for eid, attrs in outputs.items():
            if eid not in self.dummies:
                raise ValueError('Unknown entity ID "%s"' % eid)

            data[eid] = {}
            for attr in attrs:
                if attr not in self.meta['models']['DummySim']['attrs']:
                    raise AttributeError('Attribute "%s" not available' % attr)
                data[eid][attr] = self.object

        return data


def main():
    """Run our simulator and expose the "PickleSim"."""
    return mosaik_api.start_simulation(PickleSim(), 'Pickle simulator')
