import logging
import pickle
import time
import asyncio

from des_flex.flex_class import Flexibility
from src.StartValues import StartValues

from src.mosaik import MosaikAPI


def test_mango():
    # load flex
    pickle_file = 'tests/flexibility.p'
    loaded_flex = pickle.load(open(pickle_file, 'rb'))
    flex = Flexibility(
        flex_max_power=loaded_flex['flex_max_power'],
        flex_min_power=loaded_flex['flex_min_power'],
        flex_max_energy_delta=loaded_flex['flex_max_energy_delta'],
        flex_min_energy_delta=loaded_flex['flex_min_energy_delta']
    )


    # Create StartValues
    schedule = [0] * 96
    for i in [20, 21, 40, 57, 86, 90]:
        schedule[i] = 5

    start_values = StartValues(schedule=schedule, participants=[3, 4, 7, 8])

    # Create MosaikSimulator
    mosaik = MosaikAPI()
    params = {}
    mosaik.init(sid=0)

    params = {'control_id': 0}
    agents = mosaik.create(10, 'FlexAgent', **params)

    mosaik.setup_done()

    input_data = {}
    for agent in agents:
        eid = agent['eid']
        if eid == 'Agent_0':
            data = {'StartValues': start_values}
        else:
            data = {'Flexibility': flex}
        input_data[eid] = data

    mosaik.step(time=0, inputs=input_data, max_advance=0)
    time.sleep(30)


if __name__ == '__main__':
    test_mango()
