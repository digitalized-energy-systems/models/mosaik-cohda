"""
Implementation of the `mosaik API`_ for the Pickle sim.

.. _mosaik API:
   https://mosaik.readthedocs.org/en/latest/mosaik-api/high-level.html

"""
import mosaik_api
import pickle

from des_flex.flex_class import Flexibility


# The meta data that the "init()" call will return.
META = {
    'api_version': '3.0',
    'type': 'time-based',
    'models': {
        'PickleSim': {
            'public': True,
            'params': [],
            'attrs': [
                'output',  # output
            ],
        },
    },
}
STEP_SIZE = 15  # minutes


class PickleSim(mosaik_api.Simulator):
    """This class implements the mosaik API."""
    def __init__(self):
        # We need to pass the META data to the parent class which will extend
        # it and store it as "self.meta":
        super().__init__(META)

        self.pickle_file = None  # File handle for wind velocities
        self.pickles = {}
        self.pickle_config = []
        self.object = None

    def init(self, sid, time_resolution=1., **sim_params):
        """*pickle_file* is a pickle file
        """
        pickle_file = sim_params['pickle_file']
        self.pickle_file = pickle_file
        loaded_flex = pickle.load(open(self.pickle_file, 'rb'))
        self.object = Flexibility(
            flex_max_power=loaded_flex['flex_max_power'],
            flex_min_power=loaded_flex['flex_min_power'],
            flex_max_energy_delta=loaded_flex['flex_max_energy_delta'],
            flex_min_energy_delta=loaded_flex['flex_min_energy_delta']
        )

        # Return our simulator's meta data to mosaik:
        return self.meta

    def create(self, num, model, **pickles_params):
        """Create *num* instances of *model*.

        Mosaik makes sure that we get valid values for *num* and *model*.
        Since we only exposed one model, we know that *model* is "flex".


        """
        n_pickle = len(self.pickles)  # Number of pickle-simulators so
        # far
        entities = []  # This will hold the entity data for mosaik
        for pickle_idx in range(n_pickle, n_pickle + num):
            # The entity ID for mosaik:
            eid = 'pickels-%s' % pickle_idx

            self.pickles[eid] = pickle_idx

            # Add entity data for mosaik
            entities.append({'eid': eid, 'type': model})

        # Return the list of entities to mosaik:
        return entities

    def setup_done(self):
        """Called once the scenario creation is done and just before the
        simulation starts.

        """

    def step(self, time, inputs, max_advance):
        """Step the simulator ahead in time.  The current simulation time is
        *time*.

        *inputs* may contain *P_Max* values set by the MAS.  It is a dict
        like::


        """
        # No Computation, output stays the same for all steps

        # We want to do our next step in STEP_SIZE minutes:
        return time + STEP_SIZE*60

    def get_data(self, outputs):
        """
            Return the data requested by mosaik (only output).  *outputs* is a
            dict like::


        Return a dict with the requested data::

            {
                'pickle-0': {
                    'output': object,
                },
                ...
            }

        """
        data = {}
        for eid, attrs in outputs.items():
            if eid not in self.pickles:
                raise ValueError('Unknown entity ID "%s"' % eid)

            data[eid] = {}
            for attr in attrs:
                if attr not in self.meta['models']['PickleSim']['attrs']:
                    raise AttributeError('Attribute "%s" not available' % attr)
                data[eid][attr] = self.object

        return data


def main():
    """Run our simulator and expose the "PickleSim"."""
    return mosaik_api.start_simulation(PickleSim(), 'Pickle simulator')
