"""
This file contains the mosaik scenario.  To start the simulation, just run this
script from the command line.

"""

import mosaik

# We have a multi-agent system, a pickle sim, loading the flex pickle; a
# dummy sim creating start values and a database:
SIM_CONFIG = {
    'MAS': {
        'python': 'mosaik_cohda.mosaik:MosaikAPI'
    },
    'PickleSim': {
        'python': 'tests.pickle_sim:PickleSim'
    },
    'DummySim': {
        'python': 'tests.dummy_sim:DummySim'
    },
    'DB': {
        # The HDF5 db should be run as a separate process:
        'cmd': 'mosaik-hdf5 %(addr)s',
    },
}

# We now define some constants with configuration for the simulator and the
# MAS.  This way, we can easily see and edit the config values without touching
# the actual scenario:

# Simulation duration:
DURATION = 3600 * 1 * 1  # 1 hour

# File with wind data *v* in *m/s* with 15min resolution:
FLEX_FILE = 'tests/flexibility.p'

# Database config
DB_PATH = 'tests/mosaik_results.hdf5'


def main():
    """Compose the mosaik scenario and run the simulation."""
    # We first create a World instance with our SIM_CONFIG:
    world = mosaik.World(SIM_CONFIG)

    # Then, we start our simulators:
    pickle_sim = world.start('PickleSim', pickle_file=FLEX_FILE)
    dummy_sim = world.start('DummySim')
    mas = world.start('MAS')

    flex_agents = []
    params = {'control_id': 0, 'time_resolution': 15*60}  # Control ID
    # defines, which agent starts
    # negotiation and needs startValues

    # We create one pickle and flex agent pair at a time and immediately
    # connect
    # them.
    for n in range(10):  # Iterate over the config sets
        flex = pickle_sim.PickleSim()
        agent = mas.FlexAgent(**params)
        world.connect(flex, agent, ('output', 'Flexibility'),
                      async_requests=True)

        if n == params['control_id']:
            # also connect to dummy_sim
            dummy = dummy_sim.DummySim()
            world.connect(dummy, agent, ('StartValues', 'StartValues'),
                          async_requests=True)

        # Remember the FlexAgents entity for connecting it to the DB later:
        flex_agents.append(agent)

    # Start the database process and connect all FlexAgents to it:
    db = world.start('DB', step_size=60*15, duration=DURATION)
    hdf5 = db.Database(filename=DB_PATH)
    mosaik.util.connect_many_to_one(world, flex_agents, hdf5, 'FlexSchedules')

    # Run the simulation
    world.run(DURATION)


if __name__ == '__main__':
    main()
