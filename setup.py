from setuptools import setup

# keep this file in order to be able to run "pip install -e path/fmm" for better use of this project inside others
setup(name='mosaik-COHDA',
      version='0.0.1',
      description='An simulator for mosaik which enables the use of '
                  'mango-COHDA with mosaik',
      url='https://gitlab.com/zdin-zle/models/mosaik-cohda',
      author='Stephan Ferenz, Rico Schrage')
